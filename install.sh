#!/bin/sh
#

setup_color() {
	# Only use colors if connected to a terminal
	if [ -t 1 ]; then
		RED=$(printf '\033[31m')
		GREEN=$(printf '\033[32m')
		YELLOW=$(printf '\033[33m')
		BLUE=$(printf '\033[34m')
		BOLD=$(printf '\033[1m')
		RESET=$(printf '\033[m')
	else
		RED=""
		GREEN=""
		YELLOW=""
		BLUE=""
		BOLD=""
		RESET=""
	fi
}

install_base_packages() {
    sudo apt-get install -y wget curl vim git zip bzip2 fontconfig python g++ libpng-dev build-essential software-properties-common
}

install_jdk() {
    wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo apt-key add -
    sudo add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
    sudo apt-get install -y adoptopenjdk-11-hotspot
}

install_nodejs() {
    wget https://nodejs.org/dist/v10.16.0/node-v10.16.0-linux-x64.tar.gz -O /tmp/node.tar.gz
    sudo tar -C /usr/local --strip-components 1 -xzf /tmp/node.tar.gz
    sudo npm install -g npm
}

install_sdkman() {
    curl -s "https://get.sdkman.io" | bash
}

install_zsh() {
     sudo apt-get install -y zsh
}

install_oh_my_zsh() {
    sudo sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
}

install_docker_ce() {
    #Uninstall old versions
    sudo apt-get remove docker docker-engine docker.io containerd runc
    
    #Update the apt package index:
    sudo apt-get update

    #Install packages to allow apt to use a repository over HTTPS:
    sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common

    #Add Docker�s official GPG key:
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    
    #Set up the stable repository.
    sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
       
    #Update the apt package index:
    sudo apt-get update
    
    #Install the latest version of Docker CE and containerd,
    sudo apt-get install docker-ce docker-ce-cli containerd.io

}

install_docker_compose() {
    #download the current stable release of Docker Compose
    sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    
    #Apply executable permissions to the binary
    sudo chmod +x /usr/local/bin/docker-compose
}

install_portainer() {
    sudo docker volume create portainer_data
    sudo docker run -d -p 9001:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
}

install_gitKraken() {
    wget https://release.gitkraken.com/linux/gitkraken-amd64.deb 
    sudo dpkg -i gitkraken-amd64.deb
}

install_angular_cli() {
    sudo npm install -g @angular/cli
}

install_postman() {
    cd /tmp || exit
    echo "Downloading Postman ..."
    wget -q https://dl.pstmn.io/download/latest/linux?arch=64 -O postman.tar.gz
    tar -xzf postman.tar.gz
    rm postman.tar.gz

    echo "Installing to opt..."
    if [ -d "/opt/Postman" ];then
        sudo rm -rf /opt/Postman
    fi
    sudo mv Postman /opt/Postman

    echo "Creating symbolic link..."
    if [ -L "/usr/bin/postman" ];then
        sudo rm -f /usr/bin/postman
    fi
    sudo ln -s /opt/Postman/Postman /usr/bin/postman

    echo "Creating .desktop file..."
    if [ -e "/usr/share/applications/postman.desktop" ];then
        sudo rm /usr/share/applications/postman.desktop
    fi
    sudo mv Postman.desktop /usr/share/applications/postman.desktop

    echo "Installation completed successfully."
    echo "You can use Postman!"
}

install_jetbrains_toolbox() {
    wget -cO jetbrains-toolbox.tar.gz "https://data.services.jetbrains.com/products/download?platform=linux&code=TBA"
    tar -xzf jetbrains-toolbox.tar.gz
    DIR=$(find . -maxdepth 1 -type d -name jetbrains-toolbox-\* -print | head -n1)
    cd $DIR
    ./jetbrains-toolbox
    cd ..
    rm -r $DIR
    rm jetbrains-toolbox.tar.gz
}

setup_workspace() {
    mkdir /home/jlapert/workspace
    chown -R jlapert:jlapert /home/jlapert/workspace
}

main() {

	setup_color

	printf "$BLUE"
	cat <<-'EOF'         

            
	EOF
	printf "$RESET"
    
    install_base_packages

    install_jdk

    install_nodejs
    
    install_sdkman

    install_zsh
    
    install_oh_my_zsh
    
    install_docker_ce
    
    install_portainer
    
    install_gitKraken
    
    install_angular_cli
    
    install_postman

    install_jetbrains_toolbox

    setup_workspace

    printf "$BLUE"
	cat <<-'EOF'
    Congratulations, setup is complete!
	EOF
	printf "$RESET"
}

main "$@"