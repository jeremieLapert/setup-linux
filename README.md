# Ubuntu setup dev env


---

## Using `curl`

Paste the following command into the terminal to install or update Automad in the current working directory.

    bash <(curl -s https://bitbucket.org/jeremieLapert/setup-linux/raw/master/install.sh) 
    
By default, that command will install the latest stable release. 
You can also specify a tag or branch to be installed like this:

    bash <(curl -s https://bitbucket.org/jeremieLapert/setup-linux/raw/develop/install.sh) 

## Using `wget`

In case you don't have `curl` installed on your system, you can use `wget` instead:

    bash <(wget -O - https://bitbucket.org/jeremieLapert/setup-linux/raw/master/install.sh)

---

Released under the MIT license.